﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Naloga_1
{
    class Program
    {
        public static string projectPot = @"C:\Naloga1\"; // Pot projekta
        public static string artikliDatoteka = projectPot + @"artikli.txt"; // Datoteka za artikle

        public static void BarvitiIzpis(string sporocilo, ConsoleColor barva)
        {
            Console.ForegroundColor = barva;
            Console.WriteLine(sporocilo);
            Console.ResetColor();
        }

        public static void BarvitiIzpisNapaka(Exception ex)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(ex.Message);
            Console.ResetColor();
        }

        public static void BarvitiIzpisUspeh(string sporocilo)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(sporocilo);
            Console.ResetColor();
        }
        public static void InterpreterskaVrstica(string sporocilo, ConsoleColor barva)
        {
            Console.ForegroundColor = barva;
            Console.Write(sporocilo);
            Console.ResetColor();
        }
        public static int ProjectInit()
        {
            if (Directory.Exists(projectPot) == false) // Preverimo ce mapa obstaja
            {
                try
                {
                    // Ce mapa ne obstaja jo kreiramo
                    Directory.CreateDirectory(projectPot);
                    BarvitiIzpisUspeh("Uspešno sem kreiral mapo: " + projectPot);
                }
                catch (Exception ex)
                {
                    BarvitiIzpisNapaka(ex);
                    return -1;
                }
            }

            if (File.Exists(artikliDatoteka) == false) // Preverimo ce datoteka obstaja
            {
                // Ce datoteka ne obstaja jo kreiramo
                try
                {
                    File.Create(artikliDatoteka);
                    BarvitiIzpisUspeh("Uspešno sem kreiral datoteko: " + artikliDatoteka);
                }
                catch (Exception ex)
                {
                    BarvitiIzpisNapaka(ex);
                    return -1;
                }
            }
            return 0;
        }

        static void Main(string[] args)
        {
            //Console.WriteLine(artikliDatoteka);
            if (ProjectInit() == 0) // All is good
            {
                int izbira = 0;
                bool izbiraPravilno = false;

                string meni = "1) Izpise vse artikle\n" +
                    "2) Dodaj artikel\n" +
                    "3) Poisci artikel\n" +
                    "4) Uredi artikel";

            Zacetek:
                    BarvitiIzpis("ARTIKLI", ConsoleColor.Blue);
                    BarvitiIzpis("------------------------------------------", ConsoleColor.White);
                    BarvitiIzpis(meni, ConsoleColor.White);
                    Console.WriteLine();

                    InterpreterskaVrstica("izbira>> ", ConsoleColor.Cyan);
                    izbiraPravilno = int.TryParse(Console.ReadLine(), out izbira);

                    if (izbiraPravilno)
                    {
                        switch (izbira)
                        {
                        case 1: // Izpise vse artikle 
                            Console.WriteLine("Vsi artikli");

                            StreamReader bralec = new StreamReader(Program.artikliDatoteka);

                            string[] podatki;
                            string vrstica; // = bralec.ReadLine();
                            Artikel artikel = new Artikel();

                            while ((vrstica = bralec.ReadLine()) != null) //vrstica != null
                            {
                                podatki = vrstica.Split(';');
                                /*
                                    private int _id;
                                    private string _ime;
                                    private double _cena;
                                    private Zaloga _zaloga;
                                    private string _dobavitelj; // lazje, hitreje    
                                */

                                artikel.Id = int.Parse(podatki[0]);
                                artikel.Ime = podatki[1];
                                artikel.Cena = double.Parse(podatki[2]);
                                artikel.Zaloga = int.Parse(podatki[3]); // moramo cast-at v enum Zaloga
                                artikel.Dobavitelj = podatki[4];

                                Console.WriteLine(artikel.ToString());
                            }

                            bralec.Close(); // zapremo stream
                            goto Zacetek; //gremo na zacetek
                            //break;
                        case 2: // Dodaj artikel
                            Console.WriteLine("Dodaj artikel");

                            StreamWriter pisatelj = new StreamWriter(artikliDatoteka, true);

                            int id;
                            string ime;
                            double cena;
                            int zaloga;
                            string dobavitelj;

                        VpisID:
                            //AUTO_INCREMENT se zaenkrat ni implementiran...
                            BarvitiIzpis("Vpisi Id: ", ConsoleColor.White);

                            if (int.TryParse(Console.ReadLine(), out id) == false)
                            {
                                BarvitiIzpis("Nepravilni format id-ja!", ConsoleColor.Red);
                                goto VpisID;
                            }

                        VpisIme:
                            BarvitiIzpis("Vpisi ime: ", ConsoleColor.White);
                            ime = Console.ReadLine();

                            if (ime.Length == 0)
                            {
                                BarvitiIzpis("Ime ne sme biti prazno!", ConsoleColor.Red);
                                goto VpisIme;
                            }

                        VpisCena:
                            BarvitiIzpis("Vpisi ceno: ", ConsoleColor.White);

                            if (double.TryParse(Console.ReadLine(), out cena) == false)
                            {
                                BarvitiIzpis("Nepravilni format cene!", ConsoleColor.Red);
                                goto VpisCena;
                            }

                        VpisZaloga:
                            BarvitiIzpis("Vpisi zalogo: ", ConsoleColor.White);

                            if (Enum.TryParse(Console.ReadLine().ToUpper().Trim(), out zaloga))
                            {
                                BarvitiIzpis("Format zaloge je napacen!", ConsoleColor.Red);
                                goto VpisZaloga;
                            }

                        VpisDobavitelja:
                            BarvitiIzpis("Vpisi dobavitelja: ", ConsoleColor.White);
                            dobavitelj = Console.ReadLine();

                            if (dobavitelj.Length == 0)
                            {
                                BarvitiIzpis("Dobavitelj ne sme biti prazen!", ConsoleColor.Red);
                                goto VpisDobavitelja;
                            }

                            //ZAPISEMO V DATOTEKO
                            try
                            {
                                pisatelj.WriteLine(new Artikel(id, ime, cena, zaloga, dobavitelj).ToCSV());
                                BarvitiIzpisUspeh("Artikel je bil uspešno zapisan!");
                            }
                            catch (Exception ex)
                            {
                                BarvitiIzpisNapaka(ex);
                            }
                            pisatelj.Close();
                            goto Zacetek;
                            //break;
                        case 3: // Uredi artikel
                            Console.WriteLine("Poisci artikle");

                            StreamReader bralecArtiklov = new StreamReader(artikliDatoteka);
                            List<Artikel> vsiArtikli = new List<Artikel>();
                            Artikel prebraniArtikel = new Artikel();

                            string[] artikelPodatki;
                            string vrsticaArtikel; // = bralec.ReadLine();
                            

                            while ((vrsticaArtikel = bralecArtiklov.ReadLine()) != null) //vrstica != null
                            {
                                artikelPodatki = vrsticaArtikel.Split(';');
                                /*
                                    private int _id;
                                    private string _ime;
                                    private double _cena;
                                    private Zaloga _zaloga;
                                    private string _dobavitelj; // lazje, hitreje    
                                */

                                prebraniArtikel.Id = int.Parse(artikelPodatki[0]);
                                prebraniArtikel.Ime = artikelPodatki[1];
                                prebraniArtikel.Cena = double.Parse(artikelPodatki[2]);
                                prebraniArtikel.Zaloga = int.Parse(artikelPodatki[3]); 
                                prebraniArtikel.Dobavitelj = artikelPodatki[4];

                                Console.WriteLine("Blaaaaa");
                                Console.WriteLine(prebraniArtikel.ToString());
                                Console.WriteLine("Press 'Enter'");
                                Console.ReadLine();

                                vsiArtikli.Add(new Artikel(int.Parse(artikelPodatki[0]), artikelPodatki[1], double.Parse(artikelPodatki[2]), int.Parse(artikelPodatki[3]), artikelPodatki[4]));

                                //vsiArtikli.Add(prebraniArtikel); // Doda na listo

                                //prebraniArtikel = null;
                                //Console.WriteLine(artikel.ToString());
                            }

                            bralecArtiklov.Close(); // se znebimo objekta

                            Console.WriteLine("Prebrani artilki: ");
                            vsiArtikli.ForEach(a => Console.WriteLine(a.ToString()));
                            

                            int vpisiZalogo;
                            string vpisiDobavitelja;

                            Zaloga:
                            Console.WriteLine("Vpisi zalogo: ");
                            if (int.TryParse(Console.ReadLine(), out vpisiZalogo) == false)
                            {
                                BarvitiIzpis("Zaloga ni bila v pravilnem formatu!", ConsoleColor.Red);
                                goto Zaloga;
                            }

                        Dobavitelj:
                            Console.WriteLine("Vpisi dobavitelja: ");
                            vpisiDobavitelja = Console.ReadLine().Trim();
                            if (vpisiDobavitelja.Length == 0)
                            {
                                BarvitiIzpis("Dobavitelj ne sme biti prazen!", ConsoleColor.Red);
                                goto Dobavitelj;
                            }

                            /*
                             * TODO: Query
                                Poiščite izdelke, ki jih dobavlja izbran dobavitelj, in med najdenimi poiščite tiste, 
			                    ki imajo zalogo manj kot izbrano število.
			                    Izbrane artikle izpišite na konzolo in nov tekstovni dokument. 
                             */
                            List<Artikel> filtriraniArtikli = vsiArtikli.FindAll(a => a.Zaloga < vpisiZalogo && a.Dobavitelj == vpisiDobavitelja); // poisce artikle glede na query

                            Console.WriteLine("Našel sem artikle: ");
                            filtriraniArtikli.ForEach(a => BarvitiIzpis(a.ToString(), ConsoleColor.Yellow));

                            string vsebina = ""; // string za filtrirane artikle
                            foreach (var filtrirani in filtriraniArtikli)
                            {
                                vsebina += filtrirani.ToCSV() + "\n";
                            }

                            // TODO: Zapis filtriranih artiklov v datoteko
                            StreamWriter pisateljFiltrirani = new StreamWriter(projectPot + @"filtrirani.txt");

                            try
                            {
                                pisateljFiltrirani.WriteLine(vsebina); // zapisemo filtrirane artikle
                                BarvitiIzpisUspeh("Zapis v datoteko je bil uspešen!");
                            }
                            catch (Exception ex)
                            {
                                BarvitiIzpisNapaka(ex);
                            }
                            finally
                            {
                                pisateljFiltrirani.Close(); // Obvezno moramo zapreti StreamWriter-ja
                            }

                            goto Zacetek;
                            //break;
                        case 4:
                            Console.WriteLine("Uredi artikel");
                        Popust:
                            Console.WriteLine("Vpišite odstotek popusta!");
                            double odstotek = 0;

                            if (double.TryParse(Console.ReadLine().Trim(), out odstotek) == false)
                            {
                                BarvitiIzpis("Nepravilni format odstotka!", ConsoleColor.Red);
                                goto Popust;
                            }
                            else
                            {
                                if (odstotek <= 100 && odstotek >= 1) // omejimo vrednosti
                                {
                                    try
                                    {
                                        string[] data = File.ReadAllLines(artikliDatoteka);
                                        Artikel tempArtikel = new Artikel();
                                        List<Artikel> artikels = new List<Artikel>();
                                        string[] line;

                                        foreach (var podatek in data)
                                        {
                                            line = podatek.Split(';');

                                            tempArtikel.Id = int.Parse(line[0]);
                                            tempArtikel.Ime = line[1];
                                            tempArtikel.Cena = double.Parse(line[2]);
                                            tempArtikel.Zaloga = int.Parse(line[3]); 
                                            tempArtikel.Dobavitelj = line[4];

                                            artikels.Add(new Artikel(int.Parse(line[0]), line[1], double.Parse(line[2]), int.Parse(line[3]), line[4]));
                                            //artikels.Add(tempArtikel);
                                        }
                                        Console.WriteLine("Vsi artikli za znizanje cene: ");
                                        artikels.ForEach(a => Console.WriteLine(a.ToString()));

                                        int idArtikla;

                                        DolocitevPopusta:
                                        Console.WriteLine("Vpišite id artikla za določitev popusta: ");

                                        if (int.TryParse(Console.ReadLine().Trim(), out idArtikla) == false)
                                        {
                                            BarvitiIzpis("Nepravilni format id-ja", ConsoleColor.Red);
                                            goto DolocitevPopusta;
                                        }
                                        else
                                        {
                                            Artikel artikelZnizaj = artikels.Find(a => a.Id == idArtikla); // poisici artikle glede na id

                                            //TODO: Izracun popusta
                                            //double popustVrednost = artikelZnizaj.Cena * odstotek;
                                            //artikelZnizaj.Cena -= popustVrednost;

                                            double znizaj = (odstotek / 100) * artikelZnizaj.Cena;
                                            artikelZnizaj.Cena = artikelZnizaj.Cena - znizaj;

                                            Console.WriteLine("Znizana cena: " + artikelZnizaj.Cena);

                                            //@TODO: StreamWriter konstruktor               pot do datoteke            true -> zelimo append-ati podatke v datoteko
                                            StreamWriter pisateljPopust = new StreamWriter(projectPot + @"akcija.txt", true);
                                            try
                                            {
                                                pisateljPopust.WriteLine(artikelZnizaj.ToCSV());
                                                BarvitiIzpisUspeh("Artikel je bil uspešno dodan v akcijo!");
                                                goto Zacetek;
                                            }
                                            catch (Exception ex)
                                            {

                                                BarvitiIzpisNapaka(ex);

                                                Console.ReadLine();
                                                goto Zacetek;
                                            }
                                            finally
                                            {
                                                pisateljPopust.Close();
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        BarvitiIzpisNapaka(ex);
                                    }
                                }
                                else
                                {
                                    BarvitiIzpis("Odstotek mora biti med 1% in 100%!", ConsoleColor.Red);
                                    goto Zacetek;
                                }
                            }

                            break;
                        default:
                                BarvitiIzpis("Napačna izbira!", ConsoleColor.Red);
                                goto Zacetek;
                        }
                    }
                    else
                    {
                        BarvitiIzpis("Nepravilni format izbire!", ConsoleColor.Red);
                        goto Zacetek;
                    }
            }
            else
            {
                BarvitiIzpis("Program je naletel na tezavo!", ConsoleColor.Red);
            }


            




            Console.ReadLine();
        }
    }
}
