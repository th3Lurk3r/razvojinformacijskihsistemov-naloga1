﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Naloga_1
{
    public enum Zaloga
    {
        Da,
        Ne
    }
    class Artikel
    {
        private int _id;
        private string _ime;
        private double _cena;
        private int _zaloga;
        private string _dobavitelj; // lazje, hitreje
        //private Dobavitelj _dobavitelj; - Boljse, normalizirano
        public Artikel()
        {
        }

        public Artikel(int id, string ime, double cena, int zaloga, string dobavitelj)
        {
            _id = id;
            _ime = ime ?? throw new ArgumentNullException(nameof(ime));
            _cena = cena;
            _zaloga = zaloga;
            _dobavitelj = dobavitelj ?? throw new ArgumentNullException(nameof(dobavitelj));
        }

        public int Id { get => _id; set => _id = value; }
        public string Ime { get => _ime; set => _ime = value; }
        public double Cena { get => _cena; set => _cena = value; }
        public int Zaloga { get => _zaloga; set => _zaloga = value; }
        public string Dobavitelj { get => _dobavitelj; set => _dobavitelj = value; }

        public string ToCSV()
        {
            return this._id + ";" + this._ime + ";" + this._cena + ";" + this._zaloga + ";" + this._dobavitelj;
            //return base.ToString(); 
        }
        public override string ToString()
        {
            return this._id + "\t" + this._ime + "\t" + this._cena + "\t" + this._zaloga + "\t" + this._dobavitelj;
            //return base.ToString(); 
        }
    }
}
