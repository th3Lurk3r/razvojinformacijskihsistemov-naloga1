﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Naloga_1
{
    // @Deprecated - for now
    class Dobavitelj
    {
        private int _id;
        private string _naziv;

        public Dobavitelj()
        {
        }

        public Dobavitelj(int id, string naziv)
        {
            _id = id;
            _naziv = naziv ?? throw new ArgumentNullException(nameof(naziv));
        }
    }
}
